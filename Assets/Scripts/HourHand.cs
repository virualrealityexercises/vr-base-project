using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HourHand : ClockHand
{    
    public HourHand(GameObject clockHourHand) : base(clockHourHand) {}    

    public void SetInitialGrades(int gradesPerHour)
    {
        InitialGrades = System.DateTime.Now.Hour * gradesPerHour;
    }

    new public void RotateHandToInitialPosition()
    {
        base.RotateHandToInitialPosition();
    }

    public void UpdateHours(int grades)
    {
        base.UpdateHand(grades);
    }
}
