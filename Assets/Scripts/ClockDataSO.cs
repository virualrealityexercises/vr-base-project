using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ClockDataSO", menuName = "Clock Data", order = 51)]
public class ClockDataSO : ScriptableObject
{
    #region Attributes   
    [SerializeField] GameObject clock;
    private int minAndSecGrades = 6;
    private int hourGrades = 30;    
    private int maxGrades = 360;
    private int hourIndex = 1;
    private int minuteIndex = 2;
    private int secondIndex = 3;
    #endregion

    #region Getters
    public GameObject Clock { get { return clock; } }
    public int GradesPerSecond { get { return minAndSecGrades; } }
    public int GradesPerMinute { get { return minAndSecGrades; } }
    public int GradesPerHour { get { return hourGrades; } }
    public int MaxGrades { get { return maxGrades; } }
    public int HourIndex { get { return hourIndex; } }
    public int MinuteIndex { get { return minuteIndex; } }
    public int SecondIndex { get { return secondIndex; } }

    #endregion
}
