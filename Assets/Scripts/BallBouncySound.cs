using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBouncySound : MonoBehaviour
{
    AudioSource audioSource;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision other) {
       audioSource.PlayOneShot(audioSource.clip);
    }
}
