using System.Collections;
using UnityEngine;

public class Clock : MonoBehaviour
{
    [SerializeField] ClockDataSO clockData;
    private GameObject clock;
    private HourHand clockHourHand;
    private SecondHand clockSecondHand;
    private MinuteHand clockMinuteHand;
    
    void Start()
    {
        clock = Instantiate(clockData.Clock, transform.position, transform.rotation);
        StartHours();
        StartMinutes();
        StartSeconds();        
        StartCoroutine(ClockTime());
    }

    #region Initialize Clock
    private void StartHours()
    {
        clockHourHand = new HourHand(GetClockHand(clockData.HourIndex));
        clockHourHand.SetInitialGrades(clockData.GradesPerHour);
        clockHourHand.RotateHandToInitialPosition();       
    }
    private void StartMinutes()
    {
        clockMinuteHand = new MinuteHand(GetClockHand(clockData.MinuteIndex));
        clockMinuteHand.SetInitialGrades(clockData.GradesPerMinute);
        clockMinuteHand.RotateHandToInitialPosition();
    }
    private void StartSeconds()
    {        
        clockSecondHand = new SecondHand(GetClockHand(clockData.SecondIndex));
        clockSecondHand.SetInitialGrades(clockData.GradesPerSecond);
        clockSecondHand.RotateHandToInitialPosition();
    }

    private GameObject GetClockHand(int childIndex)
    {
        return clock.transform.GetChild(childIndex).gameObject;
    }
    #endregion

    #region Update Clock
    private IEnumerator ClockTime()
    {        
        while(true)
        {
            UpdateSeconds();           
            yield return new WaitForSeconds(1);
        }        
    }
    
    private void UpdateSeconds()
    {   
        clockSecondHand.UpdateSeconds(clockData.GradesPerSecond);  
        Check360Grades(clockSecondHand, true);
    }

    private void UpdateMinutes()
    {     
        clockMinuteHand.UpdateMinutes(clockData.GradesPerMinute);
        Check360Grades(clockMinuteHand, false);
    }

    private void Check360Grades(ClockHand clockHand, bool isSeconds)
    {
        if(clockHand.ActualGrades == clockData.MaxGrades)
        {
            clockHand.ActualGrades = 0;   
            if(isSeconds) { UpdateMinutes(); }        
            else { clockHourHand.UpdateHours(clockData.GradesPerHour); }             
        }
    }
    #endregion
}
