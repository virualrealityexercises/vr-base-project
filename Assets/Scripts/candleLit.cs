using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class candleLit : MonoBehaviour
{
    SpriteRenderer spriteRenderer;

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "candleLight"){
            spriteRenderer = other.gameObject
                            .GetComponent<SpriteRenderer>();
            spriteRenderer.enabled = true;
        }
    }
}
