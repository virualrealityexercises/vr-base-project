using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockHand
{
    private GameObject clockHandObject;
    private int initialGrades = 0;
    private int actualGrades = 0;
    public GameObject ClockHandObject { get; }

    public int InitialGrades
    {
        get { return initialGrades; }
        set { initialGrades = value; }
    }

    public int ActualGrades 
    {
        get { return actualGrades; }
        set { actualGrades = value; }
    }

    public ClockHand(GameObject clockHand)
    {
        clockHandObject = clockHand;
        clockHandObject.transform.localRotation = Quaternion.Euler(0,0,0);
    }

    public void RotateHandToInitialPosition()
    {
        RotateHand(InitialGrades);
    }
    public void UpdateHand(int grades)
    {
        RotateHand(grades);
        ActualGrades += grades;
    }

    private void RotateHand(int grades)
    {
        Vector3 rotationToAdd = new Vector3(grades,0,0);
        clockHandObject.transform.Rotate(rotationToAdd);
    }
}
