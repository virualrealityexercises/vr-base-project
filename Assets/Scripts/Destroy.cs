using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitForDestroy());
    }

    private IEnumerator WaitForDestroy(){
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

}
